<?php //include 'api/templates/init.php'; ?>

<?php 
	
	$date_readable = date('jS F', time());
	$date_db = date('j_n', time());
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'api/templates/head.php' ?>
	<?php include 'api/templates/assets.php' ?>

	<script type="text/javascript" src="api/ajax/attendance/script.js"></script>
</head>
<body style="background-color: #BCBCBC;">

	
	
	<div class="container">
		<?php include 'api/templates/navbar.php' ?>

		<hr>

		<div class="row d-flex justify-content-center" align="center">
			<div class="alert alert-primary" role="alert">
				Attendance for <?php echo $date_readable; ?><hr>
				<!-- db date <?php echo $date_db; ?> -->
				<div>
					<h2><span id="total_attendance">___</span></h2>
				</div>
				
				
			</div>
		</div>
		<div class="row">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span>
			  </div>
			  <input id="search" type="text" class="form-control" placeholder="search" aria-label="search" aria-describedby="basic-addon1">
			</div>
		</div>

		<div class="row d-flex justify-content-center">
			<div id="op" class="alert alert-success" role="alert"><i class="fa fa-check"></i></div>
		</div>
		<div id="data_container" class="row d-flex justify-content-center">
			<div class="alert alert-primary" role="alert">
				<i class="fa fa-arrow-up animated bounce infinite"></i> Please use the Search input
			</div>
			
		</div>

		
	</div>

	
</body>
</html>
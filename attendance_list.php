<?php //include 'api/templates/init.php'; ?>

<?php 
	
	$date_readable = date('jS F', time());
	$date_db = date('j_n', time());
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'api/templates/head.php' ?>
	<?php include 'api/templates/assets.php' ?>
	
</head>
<body style="background-color: #BCBCBC;">

	
	
	<div class="container">
		<?php include 'api/templates/navbar.php' ?>

		<hr>

		
		<div id="content">
			
		
			<div class="row d-flex justify-content-center" style="background-color: #FFFFFF;box-shadow: 0px 0px 8px #888888;">


				<h2>MasterCard Foundation Training</h2>
			</div>

			<hr>

			<button class="btn btn-primary mb-3" id="gen">Download as PDF</button>


			<div class="row" style="background-color: #FFFFFF;box-shadow: 0px 0px 8px #888888;">
					
				

					<?php
					  //session_start();
					  
					  include('api/ajax/db.php');
					 
					  //include '';

					  function date_attendances($date){
						if ((!$date) or ($date == '0')) {
							$res = '<span class="badge badge-danger"><i class="fa fa-times"></i></span>';
						} else {
							$res = '<span class="badge badge-success"><i class="fa fa-check"></i></span>';
						}
						return $res;
					  }
					
					  
					
					  $query = "SELECT * FROM mcf_registration_grp_1";
					
					  if (!$result = mysqli_query($con, $query)) {
					        exit(mysqli_error($con));
					    }
					
					    // if query results contains rows then fetch those rows 
					    if(mysqli_num_rows($result) > 0)
					    {
					      $numbering = 0;


					        echo '
					          <table class="table table-stripped table-sm" style="">
					        <thead>
					          <tr>
					            <th scope="col">#</th>
					            <th scope="col">Name</th>
					            <th scope="col">Id</th>
					            <th scope="col">Phone</th>

					            <th scope="col">25 Mon</th>
					            <th scope="col">26 Tue</th>
					            <th scope="col">27 Wed</th>
					            <th scope="col">28 Thur</th>
					            <th scope="col">29 Fri</th>
					            
					            <th scope="col">2 Mon</th>
					            <th scope="col">3 Tue</th>
					            <th scope="col">4 Wed</th>
					            <th scope="col">5 Thur</th>
					            <th scope="col">6 Fri</th>

					          </tr>
					        </thead>
					        <tbody>
					        ';
					      while($row = mysqli_fetch_assoc($result))
					      {
					        $numbering += 1;

					       
					        echo  '
						      
						      <tr>
						        <td>'.$numbering.'</td>
						        <td>'.$row["name"].'</td>
						        <td>'.$row["id_no"].'</td>
						        <td>'.$row["phone"].'</td>

						        <td class="table-success">'.date_attendances($row["25_11"]).'</td>
						        <td class="table-success">'.date_attendances($row["26_11"]).'</td>
						        <td class="table-success">'.date_attendances($row["27_11"]).'</td>
						        <td class="table-success">'.date_attendances($row["28_11"]).'</td>
						        <td class="table-success">'.date_attendances($row["29_11"]).'</td>

						        <td class="table-info">'.date_attendances($row["2_12"]).'</td>
						        <td class="table-info">'.date_attendances($row["3_12"]).'</td>
						        <td class="table-info">'.date_attendances($row["4_12"]).'</td>
						        <td class="table-info">'.date_attendances($row["5_12"]).'</td>
						        <td class="table-info">'.date_attendances($row["6_12"]).'</td>
						      </tr>
						      ';
						      }
						    echo  '
						         </tbody>
						    </table>
					      	';
					    }
					    else
					    {
					      // records now found 
					      echo  '<div class="alert alert-primary" role="alert">No data in database</div>';
					    }
					
					    //echo .= '</table>';
					
					    // echo $data;
					?>

			</div>
		</div>

		<div id="HTMLtoPDF"></div>

	</div>

	
<!-- datatables -->

	<!-- <link rel="stylesheet" href="api/libs/datatables/jquery.dataTables.min.css">
    <script type="text/javascript" src="api/libs/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="api/libs/datatables/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="api/libs/datatables/buttons.flash.min.js"></script>
    <script type="text/javascript" src="api/libs/datatables/jszip.min.js"></script>
    <script type="text/javascript" src="api/libs/datatables/pdfmake.min.js"></script>
    <script type="text/javascript" src="api/libs/datatables/vfs_fonts.js"></script>
    <script type="text/javascript" src="api/libs/datatables/buttons.html5.min.js"></script>
    <script type="text/javascript" src="api/libs/datatables/buttons.print.min.js"></script> -->


	<script type="text/javascript" src="api/ajax/attendance/script.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
	<script type="text/javascript">

		var doc = new jsPDF();

		// var elementHTML = $('#content').html();

		var specialElementHandlers = {
			'#HTMLtoPDF': function (element, renderer) {
			
				return true;
			}
		};

	// doc.fromHTML(elementHTML,15,15,{
	// 	'width': 170,
	// 	'elementHandlers': specialElementHandlers
	// }); 

		$('#gen').click(function() {
			// body...
			doc. fromHTML($('#content').html(), 15, 15, {
				'width': 170,
				'elementHandlers': specialElementHandlers
			});

			// save the pdf
			doc.save('mcf_training.pdf');

	});
		
	</script>
	
</body>
</html>
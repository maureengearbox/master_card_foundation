<?php //include 'api/templates/init.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'api/templates/head.php' ?>
	<?php include 'api/templates/assets.php' ?>

	<script type="text/javascript" src="api/ajax/messaging/script.js"></script>
</head>
<body style="background-color: #BCBCBC;">

	
	
	<div class="container">
		<?php include 'api/templates/navbar.php' ?>


		<div class="row d-flex justify-content-center" align="center">
			<div id="balance">
				<span class="badge badge-primary">Loading balance ... </span>
			</div>
		</div>

		

		<div class="row">
			
			<div class="col-lg-12">
				<div id="word_count"><div class="badge badge-warning" role="badge">0 ch, 0 msg</div></div>
			</div>
			<div class="col-lg-12">
				
			</div>
			

		</div>
		<div class="row">
			<div class="col">
				<div id="op"></div>
			</div>
		</div>

		<div class="row">
			
			<div class="col">
				<div id="users_select_no" class="alert alert-success"></div>
			</div>
				
		</div>

		<div class="row">
			<div class="input-group mb-3 col">
				  <div class="input-group-prepend">
				    <label class="input-group-text" for="users_select">Area</label>
				  </div>
				  <select class="custom-select" id="users_select">
				    <option selected value="0">Choose...</option>
				    <option value="KAMKUNJI">Kamkunji</option>
				    <option value="KARIOBANGI">Kariobangi</option>
				    <option value="NGONG ROAD">Ngong Road</option>
				  </select>
				</div>
		</div>
		<div class="row">
			<div class="input-group col">
			  <div class="input-group-prepend">
			    <span class="input-group-text">Message</span>
			  </div>
			  <textarea class="form-control" aria-label="With textarea" rows="5" id="message"></textarea>
			</div>
		</div>

		<hr>
		<div class="row d-flex justify-content-center col">
			<button type="button" class="btn btn-success" id="send_message"> <i class="fa fa-tick"></i>Send</button>
		</div>

		
	</div>

	
</body>
</html>
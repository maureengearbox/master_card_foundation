<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#">
		<!-- <img src="http://gearbox.ke/tour/assets/img/web/main.png" class="img-responsive" 
		style="height: 50px;"> -->
		<img src="api/img/web/gearbox_logo_plain.png" style="width: 40px; height: 40px; ">
	</a>

	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
		<div class="navbar-nav m-auto lead">
			<!-- <a class="nav-item nav-link active" href="#">Home <span class="sr-only">(current)</span></a> -->
			<a class="nav-item nav-link mr-1" href="students"><i class="fa fa-user"></i> Students</a>

			<a class="nav-item nav-link mr-1" href="messaging"><i class="fa fa-envelope mr-1"></i>Messaging</a>
			<!-- <a class="nav-item nav-link" href="attendance">Attendance</a> -->
			<!-- <a class="nav-item nav-link disabled" href="#">Disabled</a> -->

			<li class="nav-item dropdown">
		        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          <i class="fa fa-users mr-1"></i> Attendance
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
		          <a class="dropdown-item" href="attendance">Register</a>
		          <a class="dropdown-item" href="attendance_list">List</a>
		          
		        </div>
		      </li>
		</div>
	</div>
</nav>
<br>



<style type="text/css">
	.no-js #loader { display: none;  }
		.js #loader { display: block; position: absolute; left: 100px; top: 0; }
		#splash_screen {
			position: fixed;
			left: 0px;
			top: 0px;
			width: 100%;
			height: 100%;
			z-index: 9999;
			background: url(images/loader-64x/Preloader_2.gif) center no-repeat #fff;
		}
</style>

<div id="splash_screen"></div>

<script type="text/javascript">
	$(window).on('load', function() {
		// Animate loader off screen
		$("#splash_screen").fadeOut("slow");;
	});
</script>
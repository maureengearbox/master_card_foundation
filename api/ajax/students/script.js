var ajax_url = 'api/ajax/students/';

// read
	function read(){
		$.get(
			ajax_url + "read.php",
			{},
			function (data, status) {
	        	$("#data_container").html(data);
	    	}
	    );
	}


// add
	function add_btn(){
		clear_add();
		$("#add_modal").modal("show");
	}

	function add_submit(){
		// pull in values/variables
		var name = $("#add_name").val();
		var dob = $("#add_dob").val();
		var gender = $("#add_gender").val();
		var id_no = $("#add_id_no").val();
		var phone = $("#add_phone").val();
		var phone = $("#add_age").val();



	    $.ajax({  
	        url:ajax_url + "add.php",  
	        method:"post",  
	        data:{
	        	name:name,
	        	dob:dob,
	        	gender:gender,
	        	id_no:id_no,
	        	phone:phone,
	        	age:age
	        },  
	        dataType:"text",  
	        success:function(data)  
	        {  
	        	// console.log(data);
	        	var res = JSON.parse(data);

	        	if (res.status == 'success') {
	        		// refresh data
		            read();  

		            // alert user that record has been added
		            $('#op').html('<div class="alert alert-success" role="alert"><i class="fa fa-check"></i> Record added</div>');

		            // clear the fields
		            clear_add();

		            // close modal
		            $("#add_modal").modal("hide");
	        	} else {
	        		$('#op').html('<div class="alert alert-danger" role="alert"><i class="fa fa-warning"></i> Error !!</div>');	
	        	}

	        	
	        },
	        error: function (jqXhr, textStatus, errorThrown) {
	            //$('#_op').html(jqXhr + textStatus + errorThrown);
	            alert("Error!");
	        } 
	    });  
	}

	function clear_add(){
		$('#add_name').val('');
		$('#add_dob').val('');
		$('#add_gender').val('');
		$('#add_id_no').val('');
		$('#add_phone').val('');
		$('#add_age').val('');


	}

	

// edit
	

	// function get_details(id){
	// 	clear_edit();

		// Add User ID to the hidden field for future usage
	    // $("#edit_id").val(id);
	    // $.post(ajax_url + "get_details.php", {
	    //         id: id
	    //     },
	    //     function (data, status) {
	            // PARSE json data
	            // var res = JSON.parse(data);

	            // Assing existing values to the modal popup fields
	            // $("#ctgr_update_name").val(res.name);
	   //          $('#edit_name').val(res.name);
				// $('#edit_dob').val(res.dob);
				// $('#edit_gender').val(res.gender);
				// $('#edit_id_no').val(res.id_no);
				// $('#edit_phone').val(res.phone);
				// $('#edit_level').val(res.level);
				// $('#edit_ssn_group').val(res.ssn_group);
				// $('#edit_site').val(res.site);

	            
	           
				// $("#edit_modal").modal("show");
	           
	   //      }
	   //  );
	    // Open modal popup
	    //$("#update_modal").modal("show");
	    // document.getElementById('_update_modal').style.display='block';
	// }

	// function clear_edit(){
	// 	$('#edit_name').val('');
	// 	$('#edit_dob').val('');
	// 	$('#edit_gender').val('');
	// 	$('#edit_id_no').val('');
	// 	$('#edit_phone').val('');
	// 	$('#edit_level').val('');
	// 	$('#edit_ssn_group').val('');
	// 	$('#edit_site').val('');

	// }
// delete



$( document ).ready(function() {
	// read
    	read();

    // add
	    $('#add_btn').click(function(){
	    	add_btn();
	    	return false;
	    });

		$('#add_submit').click(function(){
	    	add_submit();
	    	return false;
	    }); 

	// edit
		 // $('#edit_submit').click(function(){
	  //   	edit_submit();
	  //   	return false;
	  //   }); 
});
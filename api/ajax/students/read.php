<?php
  //session_start();
  
  include("../db.php");
  //include '';

  

  $query = "SELECT * FROM mcf_registration_grp_1 order by id desc";

  if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }

    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
      $numbering = 0;
        $data = '
			<span class="badge badge-primary">Total : '.mysqli_num_rows($result).'</span><br><hr>

			<table class="table table-bordered">
				<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Name</th>
					<th scope="col">Gender</th>
					<th scope="col">DoB</th>
					<th scope="col">ID No</th>
					<th scope="col">Phone</th>
          <th scope="col">Age</th>



				</tr>
			</thead>
			<tbody>
        ';
      while($row = mysqli_fetch_assoc($result))
      {
        $numbering += 1;
        $data .= '
	      <tr>
	        <th scope="row">'.$numbering.'</th>
	        <td>'.$row["name"].'</td>
	        <td>'.$row["gender"].'</td>
	        <td>'.$row["dob"].'</td>
	        <td>'.$row["id_no"].'</td>
	        <td>'.$row["phone"].'</td>
          <td>'.$row["age"].'</td>


	      </tr>
      ';
      }
      $data .= '
         </tbody>
    </table>
      ';
    }
    else
    {
      // records now found 
      $data = '<div class="alert alert-primary" role="alert">No data in database</div>';
    }

    //$data .= '</table>';

    echo $data;
?>
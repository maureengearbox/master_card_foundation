<?php

require 'init.php';

 
// check request
if(isset($_POST))
{
    // get values
    $id = mysqli_real_escape_string($con, htmlspecialchars($_POST['id']));
    $serial_no = mysqli_real_escape_string($con, htmlspecialchars($_POST['serial_no']));
    $name = mysqli_real_escape_string($con, htmlspecialchars($_POST['name']));
    $description = mysqli_real_escape_string($con, htmlspecialchars($_POST['description']));
    $class = mysqli_real_escape_string($con, htmlspecialchars($_POST['class']));
    $edited = mysqli_real_escape_string($con, htmlspecialchars($_POST['edited']));
    

    $edited = $edited . ":" . time() . "_" . $_SESSION['id'];
    
 
    // Update User details
    $query = "UPDATE " . $table . " 
    			SET 
    			serial_no = '$serial_no', 
    			name = '$name', 
    			description = '$description', 
    			class = '$class',
                edited = '$edited'
    			WHERE 
    			id = '$id'";
    if (!$result = mysqli_query($con, $query)) {
        
        $data = [
            'status' => 'error',
            'message' => exit(mysqli_error($con))
        ];
    } else {
        $data = [
            'status' => 'success',
            'message' => 'one record edited'
        ];
    }
    $data = json_encode($data);
    echo $data;
}
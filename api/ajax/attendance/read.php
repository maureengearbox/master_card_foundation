<?php
  //session_start();
  
  include("../db.php");
  //include '';

  $search = $_GET['search'];
  $date_db = date('j_n', time());

  $query = "SELECT * FROM mcf_registration_grp_1 where name like '%".$search."%' or id_no like '%".$search."%' or phone like '%".$search."%'";

  if (!$result = mysqli_query($con, $query)) {
        exit(mysqli_error($con));
    }

    // if query results contains rows then featch those rows 
    if(mysqli_num_rows($result) > 0)
    {
      $numbering = 1;
      $data = '';
      while($row = mysqli_fetch_assoc($result))
      {
      	//structure query
      	if (!$row[$date_db]) {
      		$attendance_status = '<span class="badge badge-warning">Not in attendance</span>';
      		$action_btn = '<button onclick="attendance_submit('.$row["id"].')" type="button" class="btn btn-success"><i class="fa fa-check"></i></button>';
      	} else {
      		$attendance_status = '<span class="badge badge-success">In attendance</span>';
      		$action_btn = '<button onclick="attendance_cancel('.$row["id"].')" type="button" class="btn btn-outline-danger"><i class="fa fa-times"></i></button>';
      	}
      	

        $numbering += 1;
        $data .= '
	        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="">
				<div style="margin: 5px;padding: 5px;box-shadow: 0px 0px 8px #888888;background-color:#DFDFDF;">
					Name : '.$row["name"].' <br>
					Id No : '.$row["id_no"].' <br>
					Phone : '.$row["phone"].' <br>
					Attendance Status : '.$attendance_status.' <br>
					<hr>
					'.$action_btn.'
					
				</div>
			</div>
        ';
      }
      $data .= '
         
      ';
    }
    else
    {
      // records now found 
      $data = '<div class="alert alert-primary" role="alert">No data in database</div>';
    }

    //$data .= '</table>';

    echo $data;
?>


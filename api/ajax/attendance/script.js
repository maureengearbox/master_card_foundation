	ajax_url = 'api/ajax/attendance/';

	function read(){
		var search = $("#search").val();
		$.get(
			ajax_url + "read.php",
			{search:search},
			function (data, status) {
	        	$("#data_container").html(data);
	        	console.log("reading");
	    	}
	    );
	    total_attendance();
	}

	function total_attendance(){
		$.ajax({  
		    url:ajax_url + "total_attendance.php",  
		    method:"post",  
		    data:{},  
		    dataType:"text",  
		    success:function(data)  
		    {  
		          $('#total_attendance').html(data);  
		    },
		    error: function (jqXhr, textStatus, errorThrown) {
		        console.log(data);
		        //$('#acc_in_username_op').html(jqXhr + textStatus + errorThrown);
		    } 
		});  
	}


	function attendance_submit(id){
		
	 
	    // Update the details by requesting to the server using ajax
	    $.post(ajax_url + "attendance_submit.php", {
	            id: id	            
	        },
	        function (data, status) {
	        	var res = JSON.parse(data);
	        	if (res.status == 'success') {
	        		read();
	        	} else {

	        	}
	            
	        }
	    );
	}

	function attendance_cancel(id){
		
	 
	    // Update the details by requesting to the server using ajax
	    $.post(ajax_url + "attendance_cancel.php", {
	            id: id	            
	        },
	        function (data, status) {
	        	var res = JSON.parse(data);
	        	if (res.status == 'success') {
	        		read();
	        	} else {

	        	}
	            
	        }
	    );
	}


$( document ).ready(function() {
	total_attendance()
    $('#search').on('keyup', function(){
    	read();
    });

    // alert("runnung");
});
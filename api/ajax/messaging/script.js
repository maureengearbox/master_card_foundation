
		var ajax_url = 'api/ajax/messaging/';

		function read_balance(){
			$.ajax({  
			    url:"api/ajax/messaging/balance.php",  
			    method:"post",  
			    data:{},  
			    dataType:"text",  
			    success:function(data)  
			    {  
			          $('#balance').html('<div class="alert alert-primary" role="alert"> Airtime Balance : '+ data + '</div>' );  
			    },
			    error: function (jqXhr, textStatus, errorThrown) {
			        console.log(data);
			        //$('#acc_in_username_op').html(jqXhr + textStatus + errorThrown);
			    } 
			});  
		}

		function message_count(character_count){
			if (character_count <= 160) {
	    		var message_count = 1;
	    	} else if (character_count <= 320) {
	    		var message_count = 2;
	    	} else if (character_count <= 480) {
	    		var message_count = 3;
	    	} else if (character_count <= 640) {
	    		var message_count = 4;
	    	}  else if (character_count > 640) {
	    		// var message_count = '<span class="badge badge-danger"><i class="fa fa-warning"></i> Message count maxed out</span>';
	    		// $('#send_message').hide();
	    	} 
	    	return message_count
		}


		$( document ).ready(function() {
		    
		    read_balance();

		    $('#message').keyup(function(){
		    	var character_count = $("#message").val().length;
		    	$('#send_message').show();
		    	var message_count_no = message_count(character_count);
		    	
		    	// message += 1;
		    	$("#word_count").html('<div class="badge badge-primary" role="badge"><b>' + character_count + '</b> ch, ' + message_count_no + ' msg </div>');
		    });

		    // $('#search').keyup(function(){
		    // 	var search = $("#search").val();

		    // 	if (!search) {
		    // 		$('#search_list').html(" ");  
		    // 	} else {
		    	
		    // 	$.ajax({  
		    // 	    url:"api/ajax/messaging/search.php",  
		    // 	    method:"post",  
		    // 	    data:{search:search},  
		    // 	    dataType:"text",  
		    // 	    success:function(data)  
		    // 	    {  
		    // 	          $('#search_list').html(data);  
		    // 	    },
		    // 	    error: function (jqXhr, textStatus, errorThrown) {
		    // 	        console.log(data);
		    // 	        //$('#acc_in_username_op').html(jqXhr + textStatus + errorThrown);
		    // 	    } 
		    // 	});  
		    // 	}

		    // });

		    $('#users_select').on('change', function(){
		    	// console.log("changed ") ;
		    	var users_select = $(this).val();
		    	// console.log(users_select);

		    	$.ajax({  
		    	    url:ajax_url + "users_select_no.php",  
		    	    method:"post",  
		    	    data:{users_select:users_select},  
		    	    dataType:"text",  
		    	    success:function(data)  
		    	    {  
		    	          $('#users_select_no').html(data);  
		    	    },
		    	    error: function (jqXhr, textStatus, errorThrown) {
		    	        console.log(data);
		    	        //$('#acc_in_username_op').html(jqXhr + textStatus + errorThrown);
		    	    } 
		    	});  
		    });

		    $('#send_message').click(function(){
		    	var message = $('#message').val();
		    	var users_select_no = $('#users_select_no').val();
		    	var balance = $('#balance').val();

		    	if (!message || !users_select || (users_select == 0)) {
		    		$('#op').html('<div class="alert alert-danger animated bounce" role="alert"><i class="fa fa-warning"></i> Please select all options</div>');
		    	} else {
		    		$.ajax({  
		    		    url:"file.php",  
		    		    method:"post",  
		    		    data:{name:name, subject:subject, message:message},  
		    		    dataType:"text",  
		    		    success:function(data)  
		    		    {  
		    		          $('#result').html(data);  
		    		    },
		    		    error: function (jqXhr, textStatus, errorThrown) {
		    		        console.log(data);
		    		        //$('#acc_in_username_op').html(jqXhr + textStatus + errorThrown);
		    		    } 
		    		});  
		    	}
		    });
		});

		
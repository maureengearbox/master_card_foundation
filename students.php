<?php //include 'api/templates/init.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'api/templates/head.php' ?>
	<?php include 'api/templates/assets.php' ?>

	<script type="text/javascript" src="api/ajax/students/script.js"></script>
</head>
<body style="background-color: #BCBCBC;">

	
	
	<div class="container">
		<?php include 'api/templates/navbar.php' ?>


		<div class="row d-flex justify-content-center">

			<button type="button" class="btn btn-primary " id="add_btn"><i class="fa fa-plus"></i></button>
		</div>
		


		<div class="row" id="op"></div><br>

		<div class="row" id="data_container" style="background-color: #FFFFFF;box-shadow: 0px 0px 8px #888888;padding:10px;margin:10px;"></div>

		
		
		<!-- Add Modal -->
		<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="add_modalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="add_modalLabel"><i class="fa fa-plus"></i> Add</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<div class="row">
		      		<!-- name -->
		      		<div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">name</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_name">
		      		</div>

		      		<!-- dob -->
		      		<div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">dob</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_yob">
		      		</div>

		      		<!-- gender-->
		      		<div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">gender</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_gender">
		      		</div>

		      		<!-- id no -->
		      		<div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">ID No</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_id_no">
		      		</div>

		      		<!-- phone -->
		      		<div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">Phone</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_phone">
		      		</div>

		      		<!-- age -->
		      		<div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">Age</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_age">
		      		</div>

		      		<!-- level -->
		      		<!-- <div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <span class="input-group-text" id="basic-addon1">Level</span>
		      		  </div>
		      		  <input type="text" class="form-control" placeholder="" aria-label="" aria-describedby="basic-addon1" id="add_level">
		      		</div> -->


		      		<!-- level -->
		      		<!-- <div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <label class="input-group-text" for="add_site">Site</label>
		      		  </div>
		      		  <select class="custom-select" id="add_site">
		      		    <option selected>Choose...</option>
		      		    <option value="1">Kariobangi</option>
		      		    <option value="2">Kamukunji</option>
		      		    <option value="3">Ngong Road</option>
		      		  </select>
		      		</div> -->

		      		<!-- hcd session -->
		      		<!-- <div class="input-group mb-3">
		      		  <div class="input-group-prepend">
		      		    <label class="input-group-text" for="add_ssn_group">HCD Session Group</label>
		      		  </div>
		      		  <select class="custom-select" id="add_site">
		      		    <option selected>Choose...</option>
		      		    <option value="1">1</option>
		      		    <option value="2">2</option>
		      		  </select>
		      		</div> -->
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
		        <button type="button" class="btn btn-primary" id="add_submit">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div>

		<!-- <div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="add_modalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="add_modalLabel"><i class="fa fa-pencil"></i> Edit</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      	<div class="row">
		      		
		      	</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="button" class="btn btn-primary">Save changes</button>
		      </div>
		    </div>
		  </div>
		</div> -->

		<!-- Edit Modal -->

		<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="edit_modalLabel" aria-hidden="true">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h5 class="modal-title" id="add_modalLabel">
				        	<span class="fa-stack fa-lg">
				        	   <i class="fa fa-square-o fa-stack-2x"></i>
				        	   <i class="fa fa-pencil fa-stack-1x"></i>
				        	 </span>
				        	  Student
				     	</h5>
				        <button id="close_add_modal" type="button" class="btn btn-outline-danger" data-dismiss="modal" aria-label="Close">
				          <span aria-hidden="true"><i class="fa fa-times"></i></span>
				        </button>
				      </div>
				      <div class="modal-body">
				      	<div class="row">
				      		<!-- <div id="edit_op"></div> -->
				      	</div>

				      	<!-- id -->
				      	<input type="" name="" id="edit_id_no" style="display: none;">
				      	

				      	<!-- name -->
				      	<i class="fa fa-user"></i> name 
				      	<div class="input-group mb-3">
				      	  <input id="edit_name" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="">
				      	</div>

				      	<!-- dob -->
				      	<i class="fa fa-user"></i> YoB
				      	<div class="input-group mb-3">
				      	  <input id="edit_yob" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="">
				      	</div>

				      	<!-- gender -->
				      	<i class="fa fa-at"></i> Gender
				      	<div class="input-group mb-3">
				      	  <input id="edit_gender" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="">
				      	</div>

				      	<!-- phone -->
				      	<i class="fa fa-phone"></i> Phone
				      	<div class="input-group mb-3">
				      	  <input id="edit_phone" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="">
				      	</div>

				      	<!-- id_no -->
				      	<i class="fa fa-phone"></i> ID No
				      	<div class="input-group mb-3">
				      	  <input id="edit_id_no" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="">
				      	</div>


				      	<!-- age -->
				      	<i class="fa fa-phone"></i> Age
				      	<div class="input-group mb-3">
				      	  <input id="edit_age" type="text" class="form-control" placeholder="" aria-label="" aria-describedby="">
				      	</div>

				      	<!-- level -->
				      	<!-- <i class="fa fa-tags"></i> Level
				      	<div class="input-group mb-3">
				      	  <select class="custom-select" id="edit_level"> -->
				      	    <!-- <option selected></option> -->
				      	    <!-- <option>Up-Skiller</option>
				      	    <option>Business Owner</option> -->
				      	    <!-- <option value="c">Type C</option> -->
				      	    
				      	  </select>
				      	</div>

				      	<!-- level -->
				      	<!-- <i class="fa fa-tags"></i> HCD Sessions
				      	<div class="input-group mb-3">
				      	  <select class="custom-select" id="edit_ssn_group"> -->
				      	    <!-- <option selected></option> -->
				      	    <!-- <option value="a">1</option>
				      	    <option value="b">2</option> -->
				      	    <!-- <option value="c">Type C</option> -->
				      	    
				      	  </select>
				      	</div>   	

				      </div>
				      <div class="modal-footer">
				        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
				        <button type="button" class="btn btn-success" id="edit_submit" ><i class="fa fa-check"></i></button>
				        <div class="edit_op"></div>
				      </div>
				    </div>
				  </div>
				</div>


		
	</div>

	
</body>
</html>